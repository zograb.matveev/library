package am.matveev.spring.Library.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "Books")
@Getter
@Setter
public class BooksEntity{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "book")
    private String book;

    @Column(name = "author")
    private String author;

    @ManyToMany(mappedBy = "books")
    private List<ReaderEntity> reader;
}
