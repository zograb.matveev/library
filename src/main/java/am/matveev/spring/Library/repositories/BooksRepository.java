package am.matveev.spring.Library.repositories;

import am.matveev.spring.Library.entities.BooksEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BooksRepository extends JpaRepository<BooksEntity,Long>{
    boolean existsByAuthorAndBook(String author,String book);

    Optional<BooksEntity> findByBookAndAuthor(String book, String author);
}
