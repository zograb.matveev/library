package am.matveev.spring.Library.repositories;

import am.matveev.spring.Library.entities.ReaderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReaderRepository extends JpaRepository<ReaderEntity,Long>{

    boolean existsByNameAndSurname(String name,String surname);

    Optional<ReaderEntity> findByEmail(String email);
}
