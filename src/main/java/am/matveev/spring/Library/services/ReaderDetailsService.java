package am.matveev.spring.Library.services;

import am.matveev.spring.Library.entities.ReaderEntity;
import am.matveev.spring.Library.repositories.ReaderRepository;
import am.matveev.spring.Library.securiry.UsersDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ReaderDetailsService implements UserDetailsService{

    private final ReaderRepository readerRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        Optional<ReaderEntity> reader = readerRepository.findByEmail(username);

        if(reader.isEmpty()){
            throw new UsernameNotFoundException("Reader not found");
        }
        return new UsersDetails(reader.get());
    }
}
