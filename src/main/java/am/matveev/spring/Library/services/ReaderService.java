package am.matveev.spring.Library.services;

import am.matveev.spring.Library.dto.BooksDTO;
import am.matveev.spring.Library.dto.ReaderDTO;
import am.matveev.spring.Library.entities.BooksEntity;
import am.matveev.spring.Library.entities.ReaderEntity;
import am.matveev.spring.Library.exceptions.BookAlreadyExistException;
import am.matveev.spring.Library.exceptions.EmailAlreadyExistException;
import am.matveev.spring.Library.exceptions.ReaderExistException;
import am.matveev.spring.Library.exceptions.ReaderNotFoundException;
import am.matveev.spring.Library.mappers.BooksMapper;
import am.matveev.spring.Library.mappers.ReaderMapper;
import am.matveev.spring.Library.repositories.BooksRepository;
import am.matveev.spring.Library.repositories.ReaderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ReaderService{

    private final ReaderRepository readerRepository;
    private final BooksRepository booksRepository;
    private final ReaderMapper readerMapper;
    private final BooksMapper booksMapper;
    private final PasswordEncoder passwordEncoder;

    @Transactional(readOnly = true)
    public List<ReaderDTO> findAll(){
        List<ReaderEntity> reader = readerRepository.findAll();
        List<ReaderDTO> readerDTOS = reader.stream()
                .map(readerMapper::toDTO)
                .collect(Collectors.toList());
        return readerDTOS;
    }

    @Transactional(readOnly = true)
    public ReaderDTO findOne(long id){
        ReaderEntity reader = readerRepository.findById(id)
                .orElseThrow(ReaderNotFoundException::new);
        return readerMapper.toDTO(reader);
    }

    @Transactional
    public ReaderDTO create(ReaderDTO readerDTO){
        ReaderEntity reader = readerMapper.toEntity(readerDTO);
        if(readerRepository.existsByNameAndSurname(reader.getName(),reader.getSurname())){
            throw new ReaderExistException();
        }
        readerRepository.save(reader);
        return readerDTO;
    }

    @Transactional
    public void delete(long id){
        readerRepository.deleteById(id);
    }

    @Transactional
    public void addBookToReader(long readerId, BooksDTO booksDTO) {
        ReaderEntity reader = readerRepository.findById(readerId)
                .orElseThrow(ReaderNotFoundException::new);

        BooksEntity book = booksRepository.findByBookAndAuthor(booksDTO.getBook(), booksDTO.getAuthor())
                .orElseGet(() -> {
                    BooksEntity newBook = booksMapper.toEntity(booksDTO);
                    return booksRepository.save(newBook);
                });

        if (!reader.getBooks().contains(book)) {
            reader.getBooks().add(book);
            readerRepository.save(reader);
        }
    }

    public void registerReader(ReaderDTO readerDTO){
        ReaderEntity reader = readerMapper.toEntity(readerDTO);

        if(readerRepository.findByEmail(reader.getEmail()).isPresent()){
            throw new EmailAlreadyExistException();
        }

        reader.setPassword(passwordEncoder.encode(reader.getPassword()));
        readerRepository.save(reader);
    }
}
