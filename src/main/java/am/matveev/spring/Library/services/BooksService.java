package am.matveev.spring.Library.services;

import am.matveev.spring.Library.dto.BooksDTO;
import am.matveev.spring.Library.entities.BooksEntity;
import am.matveev.spring.Library.exceptions.BookAlreadyExistException;
import am.matveev.spring.Library.exceptions.BookNotFoundException;
import am.matveev.spring.Library.mappers.BooksMapper;
import am.matveev.spring.Library.repositories.BooksRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BooksService{

    private final BooksRepository booksRepository;
    private final BooksMapper booksMapper;

    @Transactional(readOnly = true)
    public List<BooksDTO> findAll(){
        List<BooksEntity> books = booksRepository.findAll();
        List<BooksDTO> booksDTOS = books.stream()
                .map(booksMapper::toDTO)
                .collect(Collectors.toList());
        return booksDTOS;
    }


    @Transactional(readOnly = true)
    public BooksDTO findOne(long id){
        BooksEntity books = booksRepository.findById(id)
                .orElseThrow(BookNotFoundException::new);
        return booksMapper.toDTO(books);
    }

    @Transactional
    public BooksDTO create(BooksDTO booksDTO){
        BooksEntity books = booksMapper.toEntity(booksDTO);
        if(booksRepository.existsByAuthorAndBook(books.getAuthor(), books.getBook())){
            throw new BookAlreadyExistException();
        }
        booksRepository.save(books);
        return booksDTO;
    }

    @Transactional
    public void delete(long id){
        booksRepository.deleteById(id);
    }
}
