package am.matveev.spring.Library.mappers;

import am.matveev.spring.Library.dto.ReaderDTO;
import am.matveev.spring.Library.entities.ReaderEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ReaderMapper{

    ReaderEntity toEntity(ReaderDTO readerDTO);
    ReaderDTO toDTO(ReaderEntity reader);
}
