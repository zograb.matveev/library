package am.matveev.spring.Library.mappers;

import am.matveev.spring.Library.dto.BooksDTO;
import am.matveev.spring.Library.entities.BooksEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BooksMapper{

    BooksEntity toEntity(BooksDTO booksDTO);
    BooksDTO toDTO(BooksEntity books);
}
