package am.matveev.spring.Library.controllers;

import am.matveev.spring.Library.dto.BooksDTO;
import am.matveev.spring.Library.services.BooksService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/books")
@RequiredArgsConstructor
@RestController
public class BooksController{

    private final BooksService booksService;

    @GetMapping()
    public List<BooksDTO> findAll(){
        return booksService.findAll();
    }

    @GetMapping("/{id}")
    public BooksDTO findOne(@PathVariable long id){
        return booksService.findOne(id);
    }

    @PostMapping()
    public BooksDTO create(@RequestBody @Valid BooksDTO booksDTO){
        return booksService.create(booksDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        booksService.delete(id);
    }
}
