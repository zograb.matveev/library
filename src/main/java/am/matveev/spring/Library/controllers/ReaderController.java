package am.matveev.spring.Library.controllers;

import am.matveev.spring.Library.dto.BooksDTO;
import am.matveev.spring.Library.dto.ReaderDTO;
import am.matveev.spring.Library.services.ReaderService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/reader")
@RequiredArgsConstructor
@RestController
public class ReaderController{

    private final ReaderService readerService;

    @GetMapping()
    public List<ReaderDTO> findAll(){
        return readerService.findAll();
    }

    @GetMapping("/{id}")
    public ReaderDTO findOne(@PathVariable long id){
        return readerService.findOne(id);
    }

    @PostMapping()
    public ReaderDTO create(@RequestBody @Valid ReaderDTO readerDTO){
        return readerService.create(readerDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        readerService.delete(id);
    }


    @PostMapping("/{readerId}/add-book")
    public ResponseEntity<Void> addBookToReader(@PathVariable long readerId, @RequestBody BooksDTO booksDTO) {
        readerService.addBookToReader(readerId, booksDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
