package am.matveev.spring.Library.controllers;

import am.matveev.spring.Library.dto.ReaderDTO;
import am.matveev.spring.Library.services.ReaderService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/auth")
@RestController
public class AuthController{

    private final ReaderService readerService;

    @PostMapping("/registration")
    public ResponseEntity<List<String>> registration(@RequestBody @Valid ReaderDTO readerDTO){
        readerService.registerReader(readerDTO);
        List<String> response = new ArrayList<>();
        response.add("Registration successful.");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
