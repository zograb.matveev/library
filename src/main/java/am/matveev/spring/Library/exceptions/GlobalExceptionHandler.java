package am.matveev.spring.Library.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler{

    @ExceptionHandler({BookNotFoundException.class})
    public ResponseEntity handleException(BookNotFoundException e){
        ErrorResponse response = new ErrorResponse(
                "Book whit this id wasn't found",
                LocalDateTime.now()
        );
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({BookAlreadyExistException.class})
    public ResponseEntity handleException(BookAlreadyExistException e){
        ErrorResponse response = new ErrorResponse(
                "This author or book already exist!",
                LocalDateTime.now()
        );
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ReaderNotFoundException.class})
    public ResponseEntity handleException(ReaderNotFoundException e){
        ErrorResponse response = new ErrorResponse(
                "Reader whit this id wasn't found",
                LocalDateTime.now()
        );
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ReaderExistException.class})
    public ResponseEntity handleException(ReaderExistException e){
        ErrorResponse response = new ErrorResponse(
                "This name or surname already exist!",
                LocalDateTime.now()
        );
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({EmailAlreadyExistException.class})
    public ResponseEntity handleException(EmailAlreadyExistException e){
        ErrorResponse response = new ErrorResponse(
                "This email already exist!",
                LocalDateTime.now()
        );
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }
}
