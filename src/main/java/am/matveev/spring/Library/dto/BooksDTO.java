package am.matveev.spring.Library.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BooksDTO{

    @NotEmpty(message = "Book should not be empty.")
    private String book;

    @NotEmpty(message = "Book should not be empty.")
    private String author;

    public BooksDTO(String book, String author){
        this.book = book;
        this.author = author;
    }
}
