package am.matveev.spring.Library.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ReaderDTO{

    @NotEmpty(message = "Name should not be empty")
    private String name;

    @NotEmpty(message = "Surname should not be empty")
    private String surname;

    @Email
    private String email;

    @NotEmpty(message = "Password should not be empty")
    private String password;

    private List<BooksDTO> books;
}
